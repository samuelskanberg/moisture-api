from flask import Flask, jsonify, request
from db import (
    create_tables,
    get_sensors,
    add_sensor,
    add_moisture,
    get_moisture_data,
)
from logging.config import dictConfig

dictConfig({
    'version': 1,
    'formatters': {'default': {
        'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s',
    }},
    'handlers': {'wsgi': {
        'class': 'logging.StreamHandler',
        'stream': 'ext://flask.logging.wsgi_errors_stream',
        'formatter': 'default'
    }},
    'root': {
        'level': 'INFO',
        'handlers': ['wsgi']
    }
})

app = Flask(__name__)
create_tables()

api_base = "/api/v1"

@app.route('{}/'.format(api_base))
def index():
    app.logger.info('{} {} (data: {})'.format(request.method, request.path, request.get_json()))
    return jsonify({"message": "Welcome to moisture database"}), 200
  
@app.route('{}/sensor'.format(api_base), methods=['GET'])
def get_sensors_route():
    app.logger.info('{} {} (data: {})'.format(request.method, request.path, request.get_json()))
    sensors = get_sensors()
    return jsonify(sensors), 200

@app.route('{}/sensor'.format(api_base), methods=['POST'])
def add_sensor_route():
    app.logger.info('{} {} (data: {})'.format(request.method, request.path, request.get_json()))
    request_data = request.get_json()
    description = request_data["description"]

    if description is None:
        return jsonify("Bad request"), 400

    add_sensor(description)

    response = {
        "description": description
    }
    return jsonify(response), 201

@app.route('{}/moisture/<int:sensor_id>'.format(api_base), methods=['GET'])
def get_moisture_route(sensor_id):
    app.logger.info('{} {} (data: {})'.format(request.method, request.path, request.get_json()))
    data = get_moisture_data(sensor_id)
    return jsonify(data), 200

@app.route('{}/moisture/<int:sensor_id>'.format(api_base), methods=['POST'])
def add_moisture_route(sensor_id):
    app.logger.info('{} {} (data: {})'.format(request.method, request.path, request.get_json()))
    request_data = request.get_json()
    value = request_data["value"]

    if value is None:
        return jsonify("Bad request"), 400

    add_moisture(sensor_id, value)
    
    response = {
        "sensor": sensor_id
    }
    return jsonify(response), 201