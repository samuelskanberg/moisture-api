# Moisture api

An api for saving moisture information to database.

## Setup

```bash
virtualenv -p python3 venv
source venv/bin/activate
pip install -r requirements.txt
```

## Run when testing

Run:

```bash
flask run
```

Test with curl:

```bash
# Try to get connection
curl http://127.0.0.1:5000/api/v1/
# List all sensors
curl http://127.0.0.1:5000/sensor
# Create a sensor
curl -X POST -H "Content-Type: application/json" -d '{"description": "Flower"}' http://127.0.0.1:5000/sensor
# Create moisture data
curl -X POST -H "Content-Type: application/json" -d '{"value": 123}' http://127.0.0.1:5000/moisture/1
# List moisture data for sensor 1
curl http://127.0.0.1:5000/moisture/1
```

## Run live

Run:

```bash
gunicorn --workers 4 --bind 0.0.0.0:5000 wsgi:app
```