import sqlite3

def get_connection():
    conn = sqlite3.connect('moisture.db')
    return conn

def create_tables():
    conn = get_connection()
    cur = conn.cursor()

    # Create tables
    cur.execute('''
        CREATE TABLE IF NOT EXISTS sensor 
        (id INTEGER PRIMARY KEY,
        description text)
    ''')

    cur.execute('''
        CREATE TABLE IF NOT EXISTS moisture
        (id INTEGER PRIMARY KEY,
        sensor_id integer,
        datetime text,
        value integer,
        FOREIGN KEY(sensor_id) REFERENCES sensor(id))
    ''')

    conn.commit()
    conn.close()

def get_sensors():
    conn = get_connection()
    cur = conn.cursor()
    rows = cur.execute('''
        SELECT
            id,
            description
        FROM
            sensor
    ''')
    l = []
    for row in rows:
        l.append({
            "id": row[0],
            "description": row[1]
        })
    conn.close()
    return l

def add_sensor(description):
    conn = get_connection()
    cur = conn.cursor()
    cur.execute("INSERT INTO sensor (description) VALUES (?)", (description,))
    conn.commit()
    conn.close()


def add_moisture(sensor_id, value):
    conn = get_connection()
    cur = conn.cursor()
    cur.execute("INSERT INTO moisture (sensor_id, datetime, value) VALUES (?, datetime('now'), ?)", (sensor_id, value))
    conn.commit()
    conn.close()

def get_moisture_data(sensor_id):
    conn = get_connection()
    cur = conn.cursor()
    rows = cur.execute('''
        SELECT
            id,
            sensor_id,
            datetime,
            value
        FROM
            moisture
        WHERE
            sensor_id = ?
    ''', (sensor_id, ))
    l = []
    for row in rows:
        l.append({
            "id": row[0],
            "sensor_id": row[1],
            "datetime": row[2],
            "value": row[3]
        })
    conn.close()
    return l

if __name__ == "__main__":
    sensors = get_sensors()
    print(sensors)
    moisture = get_moisture_data(5)
    print(moisture)